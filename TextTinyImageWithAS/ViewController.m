//
//  ViewController.m
//  TextTinyImageWithAS
//
//  Created by sa on 2017/10/27.
//  Copyright © 2017年 sa. All rights reserved.
//

#import "ViewController.h"
#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    CGFloat gap = 15;
    CGFloat swidth = 40;
    CGFloat x = (self.view.frame.size.width - 2 * swidth - gap) / 2;
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, 100, swidth, 30)];
    imageView.image = [UIImage imageNamed:@"UIImageView"];
    [self.view addSubview:imageView];
    
    x += swidth + gap;
    
    {
        
//        UIImage *image = [UIImage imageNamed:@"aaa"];
        UIImage *image = [UIImage imageNamed:@"ASImageNode"];
        UIImageView *imageView_forNode = [[UIImageView alloc] initWithImage:image];
        
        ASImageNode *imageNode = [[ASImageNode alloc] init];
        imageNode.contentsScale = 1;
        imageNode.image = image;
        imageNode.frame = CGRectMake(x, 100, image.size.width, image.size.height);
        [self.view addSubnode:imageNode];
        
    }
    
//    x += swidth + gap;
//
//    {
//
//        UIImage *image = [UIImage imageNamed:@"ASImageNode"];
//        UIImageView *imageView_forNode = [[UIImageView alloc] initWithImage:image];
//
//        ASImageNode *imageNode = [[ASImageNode alloc] init];
//        imageNode.image = image;
//        imageNode.frame = CGRectMake(x, 100, swidth, 30);
//        [self.view addSubnode:imageNode];
//
//    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
